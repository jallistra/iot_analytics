# ------------------------------------------------------------ #
# Ubiqum Module 3, Sprint 1
# IoT Analytics
# File: test
# Author: Danielle Jeffery
# Date: March 2020
# Version: 0.1
# ------------------------------------------------------------ #

library(lubridate)
library(dplyr)
library(ggplot2)
library(scales)

# plots power usage over one day
submeterDF %>% 
  filter(year %in% c(2009,2010),
         month == "Nov", 
         day == 22,
         hour <= 16) %>% 
  select(DateTime, submeter, watt_hr) %>% 
  group_by(year = year(DateTime), hour = hour(DateTime), submeter) %>% 
  summarise(watt_hr = sum(watt_hr)) %>% 
  ggplot(aes(x = hour, y = watt_hr, group = year, colour = year)) +
  geom_line(size = 1.3) +
  geom_hline(yintercept = 16) +
  annotate("rect", xmin = 16, xmax = Inf, ymin = -Inf, ymax = Inf, alpha = 0.2) +
  scale_x_continuous(breaks = seq(0, 23, 1)) +
  expand_limits(x = c(0, 23)) +
  theme_minimal() +
  theme(plot.title = element_text(hjust = 0.5)) +
  facet_grid(rows = vars(submeter)) +
  theme(legend.position = "none")

submeterDF %>% 
  filter(year == 2010,
         month == "Nov", 
         day == 22,
         hour <= 16) %>% 
  select(DateTime, submeter, watt_hr) %>% 
  group_by(hour = hour(DateTime), submeter) %>% 
  summarise(watt_hr = sum(watt_hr)) %>% 
  ggplot(aes(x = hour, 
             y = watt_hr, 
             group = submeter, 
             colour = submeter)) +
  geom_line(size = 1.3) +
  annotate("rect", xmin = 16, xmax = Inf, ymin = 0, ymax = Inf, alpha = 0.2) +
  scale_x_continuous(breaks = seq(0, 23, 1)) +
  expand_limits(x = c(0, 23)) +
  theme_minimal() +
  theme(legend.position = "none",
        axis.title.y = element_blank()) +
  scale_color_manual(values=c("#3d9970", "#605ca8", "#f39c12"))


# test --------------------------------------------

from_hours <- function(v) {
  
  return(v/60)
  
}

to_hours <- function(v) {
  
  return(v*60)
  
}

bum_fluff_trans <- scales::trans_new("bum_fluff", from_hours, to_hours)

# plots power usage over one day
submeterDF %>% 
  filter(year == 2010, month == "Nov", day == 22, hour < 16) %>% 
  select(DateTime, submeter, watt_hr) %>% 
  mutate(time = hour(DateTime) * 60 + minute(DateTime)) %>% 
  group_by(time, submeter) %>% 
  summarise(watt_hr = sum(watt_hr)) %>% 
  ggplot(aes(x = time, y = watt_hr, group = submeter, fill = submeter)) +
  geom_area() +
  expand_limits(x = c(0, 1440)) +
  scale_x_continuous(trans = bum_fluff_trans) +
  scale_y_continuous(breaks = seq(0, 80, 10)) +
  scale_fill_manual(values = c("#3d9970", "#605ca8", "#f39c12")) +
  annotate("rect", xmin = 960, xmax = Inf, ymin = -Inf, ymax = Inf, alpha = 0.2) +
  theme_minimal() +
  theme(legend.position = "none")

# plots the last 30 days
submeterDF %>%
  filter(as.Date(date(DateTime)) >= "2010-10-24" & as.Date(date(DateTime)) <= "2010-11-22") %>% 
  select(DateTime, watt_hr) %>%
  group_by(date = date(DateTime)) %>% 
  summarise(watt_hr = sum(watt_hr)) %>% 
  ggplot(aes(x = date, 
             y = watt_hr/1000, 
             fill = date)) +
  geom_col() +
  theme_minimal() +
  scale_x_date(labels = date_format("%d%b"), 
               breaks = date_breaks("day")) +
  theme(legend.position = "none", 
        axis.title.x = element_blank(), 
        axis.title.y = element_blank(),
        axis.text.x = element_text(angle = 45))

# plots the last 12 months
submeterDF %>%
  filter(as.Date(date(DateTime)) >= "2009-12-01" & as.Date(date(DateTime)) <= "2010-11-22") %>% 
  select(DateTime, watt_hr) %>% 
  mutate(MonthYear = as.Date(paste(as.character(year(DateTime)), 
                                   "-", 
                                   as.character(month(DateTime)), 
                                   "-01", 
                                   sep = ""))) %>% 
  group_by(MonthYear) %>% 
  summarise(watt_hr = sum(watt_hr)) %>% 
  ggplot(aes(x = MonthYear, 
             y = watt_hr/1000, 
             fill = MonthYear)) +
  geom_col() +
  scale_x_date(labels = date_format("%b"), 
               breaks = date_breaks("month")) +
  theme_minimal() +
  theme(legend.position = "none", 
        axis.title.x = element_blank(), 
        axis.title.y = element_blank())


test <- submeterDF
submeterDF$Date <- as.Date(submeterDF$Date)

