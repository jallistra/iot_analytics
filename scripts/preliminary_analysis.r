# ------------------------------------------------------------ #
# Ubiqum Module 3, Sprint 1
# IoT Analytics
# File: preliminary_analysis
# Author: Danielle Jeffery
# Date: March 2020
# Version: 0.1
# ------------------------------------------------------------ #

library(lubridate)
library(dplyr)
library(ggplot2)

# creates dataframe from csv
submeterDF <- read.csv("data/multiYearDF_prep.csv")

# converts DateTime data type from POSIX1t to POSIXct
submeterDF$DateTime <- as.POSIXct(multiYearDF_prep$DateTime,
                                  format = "%Y-%m-%d %H:%M:%S",
                                  tz = "GMT")

## Adds the time zone
attr(submeterDF$DateTime, "tzone") <- "Europe/Paris"

# plots average kitchen usage per month
submeterDF %>% 
  filter(submeter == "kitchen") %>% 
  select(year, month, watt_hr) %>%
  group_by(year, month) %>% 
  summarise(avg_watt_hr = mean(watt_hr)) %>%
  ggplot(aes(x = month, 
             y = avg_watt_hr, 
             group = year, 
             color = year)) +
  geom_line() +
  geom_point() +
  scale_color_gradientn(colours = rainbow(4)) +
  theme_minimal() +
  ggtitle("Kitchen") +
  theme(plot.title = element_text(hjust = 0.5))

# plots average laundry usage per month
submeterDF %>% 
  filter(submeter == "laundry") %>% 
  select(year, month, watt_hr) %>%
  group_by(year, month) %>% 
  summarise(avg_watt_hr = mean(watt_hr)) %>%
  ggplot(aes(x = month, 
             y = avg_watt_hr, 
             group = year, 
             color = year)) +
  geom_line() +
  geom_point() +
  scale_color_gradientn(colours = rainbow(4)) +
  theme_minimal() +
  ggtitle("Laundry") +
  theme(plot.title = element_text(hjust = 0.5))

# plots average heater and AC usage per month
submeterDF %>% 
  filter(submeter == "heater_and_AC") %>% 
  select(year, month, watt_hr) %>%
  group_by(year, month) %>% 
  summarise(avg_watt_hr = mean(watt_hr)) %>%
  ggplot(aes(x = month, 
             y = avg_watt_hr, 
             group = year, 
             color = year)) +
  geom_line() +
  geom_point() +
  scale_color_gradientn(colours = rainbow(4)) +
  theme_minimal() +
  ggtitle("Heater and AC") +
  theme(plot.title = element_text(hjust = 0.5))

# plots average kitchen usage per week day
submeterDF %>% 
  filter(submeter == "kitchen") %>% 
  mutate(weekday = weekdays(DateTime)) %>% 
  group_by(weekday) %>% 
  summarise(avg_watt_hr = mean(watt_hr)) %>%
  ggplot(aes(x = weekday, y = avg_watt_hr)) +
  geom_col(fill = "#B8860B") +
  theme_minimal() +
  ggtitle("Kitchen") +
  theme(plot.title = element_text(hjust = 0.5))

# plots average laundry usage per week day
submeterDF %>% 
  filter(submeter == "laundry") %>% 
  mutate(weekday = weekdays(DateTime)) %>% 
  group_by(weekday) %>% 
  summarise(avg_watt_hr = mean(watt_hr)) %>%
  ggplot(aes(x = weekday, y = avg_watt_hr)) +
  geom_col(fill = "#B8860B") +
  theme_minimal() +
  ggtitle("Laundry") +
  theme(plot.title = element_text(hjust = 0.5))

# plots average heater and ac usage per week day
submeterDF %>% 
  filter(submeter == "heater_and_AC") %>% 
  mutate(weekday = weekdays(DateTime)) %>% 
  group_by(weekday) %>% 
  summarise(avg_watt_hr = mean(watt_hr)) %>%
  ggplot(aes(x = weekday, y = avg_watt_hr)) +
  geom_col(fill = "#B8860B") +
  theme_minimal() +
  ggtitle("Heater and AC") +
  theme(plot.title = element_text(hjust = 0.5))

# plots total yearly energy consumption per submeter
submeterDF %>% 
  select(DateTime, submeter, watt_hr) %>% 
  group_by(year = year(DateTime), submeter) %>% 
  summarise(watt_hr = sum(watt_hr)) %>% 
  ggplot(aes(x = year, 
             y = watt_hr,
             group = submeter,
             fill = submeter)) +
  geom_bar(stat = "identity",
           colour = "black",
           position = "dodge")



