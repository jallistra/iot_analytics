# ------------------------------------------------------------ #
# Ubiqum Module 3, Sprint 1
# IoT Analytics
# File: time_series_analysis
# Author: Danielle Jeffery
# Date: March 2020
# Version: 0.1
# ------------------------------------------------------------ #

library(lubridate)
library(dplyr)
library(ggplot2)
library(forecast)

# creates dataframe from csv
submeterDF <- read.csv("data/multiYearDF_prep.csv")

# converts DateTime data type from POSIX1t to POSIXct
submeterDF$DateTime <- as.POSIXct(multiYearDF_prep$DateTime,
                                  format = "%Y-%m-%d %H:%M:%S",
                                  tz = "GMT")

## adds the time zone
attr(submeterDF$DateTime, "tzone") <- "Europe/Paris"

# kitchen ----------------------------------------

# summarises total weekly kitchen power usage
kitchen_wk_sum <- submeterDF %>% 
  filter(submeter == "kitchen") %>% 
  select(DateTime, watt_hr) %>% 
  group_by(year = year(DateTime),
           week = week(DateTime)) %>% 
  summarise(watt_hr = sum(watt_hr))

# summarises average weekly kitchen power usage
kitchen_wk_avg <- submeterDF %>% 
  filter(submeter == "kitchen") %>% 
  select(DateTime, watt_hr) %>% 
  group_by(year = year(DateTime),
           week = week(DateTime)) %>% 
  summarise(watt_hr = mean(watt_hr))

# plots total weekly kitchen power usage time series
ts(kitchen_wk_sum$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  forecast::autoplot() +
  ggtitle("Total Weekly Power Usage") +
  xlab("Year") +
  ylab("Watt Hour")

# plots average weekly kitchen power usage time series
ts(kitchen_wk_avg$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  forecast::autoplot() +
  ggtitle("Average Weekly Power Usage") +
  xlab("Year") +
  ylab("Watt Hour")

# seasonal plot: total weekly kitchen power usage
ts(kitchen_wk_sum$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  ggseasonplot() +
  ggtitle("Total Weekly Power Usage: Kitchen") +
  xlab("Week") +
  ylab("Watt Hour")

# seasonal plot: average weekly kitchen power usage
ts(kitchen_wk_avg$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  ggseasonplot() +
  ggtitle("Average Weekly Power Usage: Kitchen") +
  xlab("Week") +
  ylab("Watt Hour")

# laundry ---------------------------------------------------

# summarises total weekly laundry power usage
laundry_wk_sum <- submeterDF %>% 
  filter(submeter == "laundry") %>% 
  select(DateTime, watt_hr) %>% 
  group_by(year = year(DateTime),
           week = week(DateTime)) %>% 
  summarise(watt_hr = sum(watt_hr))

# summarises average weekly laundry power usage
laundry_wk_avg <- submeterDF %>% 
  filter(submeter == "laundry") %>% 
  select(DateTime, watt_hr) %>% 
  group_by(year = year(DateTime),
           week = week(DateTime)) %>% 
  summarise(watt_hr = mean(watt_hr))

# plots total weekly laundry power usage time series
ts(laundry_wk_sum$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  forecast::autoplot() +
  ggtitle("Total Weekly Power Usage: Laundry") +
  xlab("Year") +
  ylab("Watt Hour")

# plots average weekly laundry power usage time series
ts(laundry_wk_avg$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  forecast::autoplot() +
  ggtitle("Average Weekly Power Usage: Laundry") +
  xlab("Year") +
  ylab("Watt Hour")

# seasonal plot: total weekly laundry power usage
ts(laundry_wk_sum$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  ggseasonplot() +
  ggtitle("Total Weekly Power Usage: Laundry") +
  xlab("Week") +
  ylab("Watt Hour")

# seasonal plot: average weekly laundry power usage
ts(laundry_wk_avg$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  ggseasonplot() +
  ggtitle("Average Weekly Power Usage: Laundry") +
  xlab("Week") +
  ylab("Watt Hour")

# heater and AC ------------------------------------------

# # summarises total weekly Heater and AC power usage
heater_wk_sum <- submeterDF %>% 
  filter(submeter == "heater_and_AC") %>% 
  select(DateTime, watt_hr) %>% 
  group_by(year = year(DateTime),
           week = week(DateTime)) %>% 
  summarise(watt_hr = sum(watt_hr))

# summarises average weekly Heater and AC power usage
heater_wk_avg <- submeterDF %>% 
  filter(submeter == "heater_and_AC") %>% 
  select(DateTime, watt_hr) %>% 
  group_by(year = year(DateTime),
           week = week(DateTime)) %>% 
  summarise(watt_hr = mean(watt_hr))

# plots total weekly heater and AC power usage time series
ts(heater_wk_sum$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  forecast::autoplot() +
  ggtitle("Total Weekly Power Usage: Heater and AC") +
  xlab("Year") +
  ylab("Watt Hour")

# plots average weekly heater and AC power usage time series
ts(heater_wk_avg$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  forecast::autoplot() +
  ggtitle("Average Weekly Power Usage: Heater and AC") +
  xlab("Year") +
  ylab("Watt Hour")

# seasonal plot: total weekly heater and AC power usage
ts(heater_wk_sum$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  ggseasonplot() +
  ggtitle("Total Weekly Power Usage: Heater and AC") +
  xlab("Week") +
  ylab("Watt Hour")

# seasonal plot: average weekly heater and AC power usage
ts(heater_wk_avg$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  ggseasonplot() +
  ggtitle("Average Weekly Power Usage: Heater and AC") +
  xlab("Week") +
  ylab("Watt Hour")




