# ------------------------------------------------------------ #
# Ubiqum Module 3, Sprint 1
# IoT Analytics
# File: forecasts
# Author: Danielle Jeffery
# Date: March 2020
# Version: 0.1
# ------------------------------------------------------------ #

library(lubridate)
library(dplyr)
library(ggplot2)
library(forecast)

# creates dataframe from csv
submeterDF <- read.csv("data/multiYearDF_prep.csv")

# converts DateTime data type from POSIX1t to POSIXct
submeterDF$DateTime <- as.POSIXct(multiYearDF_prep$DateTime,
                                  format = "%Y-%m-%d %H:%M:%S",
                                  tz = "GMT")

## adds the time zone
attr(submeterDF$DateTime, "tzone") <- "Europe/Paris"

# kitchen ----------------------------------------

# forecast plot: average weekly kitchen power usage
ts(kitchen_wk_avg$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  forecast(h=58, level=c(80,95)) %>% 
  plot(xlab = "Year",
       ylab = "Watt Hour",
       main = "Average Weekly Power Usage 2011 Forecast: Kitchen")

# laundry ---------------------------------------------------

# forecast plot: average weekly laundry power usage
ts(laundry_wk_avg$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  forecast(h=58, level=c(80,95)) %>% 
  plot(xlab = "Year",
       ylab = "Watt Hour",
       main = "Average Weekly Power Usage 2011 Forecast: Laundry")

# heater and AC ------------------------------------------

# forecast plot: average weekly heater and AC power usage
ts(heater_wk_avg$watt_hr, 
   start = 2007,
   frequency = 53) %>% 
  forecast(h=58, level=c(80,95)) %>% 
  plot(xlab = "Year",
       ylab = "Watt Hour",
       main = "Average Weekly Power Usage 2011 Forecast: Heater and AC")




