# ------------------------------------------------------------ #
# Ubiqum Module 3, Sprint 1
# IoT Analytics
# File: read_db
# Author: Danielle Jeffery
# Date: March 2020
# Version: 0.1
# ------------------------------------------------------------ #

library(RMySQL)
library(dplyr)

# connects to database 
con = dbConnect(MySQL(), 
                user='deepAnalytics', 
                password='Sqltask1234!', 
                dbname='dataanalytics2018', 
                host='data-analytics-2018.cbrosir2cswx.us-east-1.rds.amazonaws.com')

# creates dataframes for year
tableList <- dbListTables(con)

for (i in 2:length(tableList)) {
  
    assign(tableList[i], 
           dbGetQuery(con, 
                      paste("select Date, 
                             Time, 
                             Sub_metering_1, 
                             Sub_metering_2, 
                             Sub_metering_3 FROM", 
                             tableList[i])))
  
}

# combines tables into one dataframe
multiYearDF <- bind_rows(yr_2007, yr_2008, yr_2009, yr_2010)

# writes dataframe to csv
write.csv(multiYearDF,"data/multiYearDF.csv", row.names = FALSE)


