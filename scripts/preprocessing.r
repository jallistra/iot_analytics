# ------------------------------------------------------------ #
# Ubiqum Module 3, Sprint 1
# IoT Analytics
# File: preprocessing
# Author: Danielle Jeffery
# Date: March 2020
# Version: 0.1
# ------------------------------------------------------------ #

library(lubridate)
library(dplyr)
library(tidyr)

# creates dataframe from csv
multiYearDF_prep <- read.csv("data/multiYearDF.csv")

# combines Date and Time values in a new column and moves to first column
multiYearDF_prep <-cbind(multiYearDF_prep,
                      paste(multiYearDF_prep$Date,
                            multiYearDF_prep$Time), 
                      stringsAsFactors=FALSE)

colnames(multiYearDF_prep)[6] <- "DateTime"

multiYearDF_prep <- multiYearDF_prep[,c(ncol(multiYearDF_prep), 
                                        1:(ncol(multiYearDF_prep)-1))]

# converts DateTime data type from POSIX1t to POSIXct
multiYearDF_prep$DateTime <- as.POSIXct(multiYearDF_prep$DateTime,
                                        format = "%Y-%m-%d %H:%M:%S",
                                        tz = "GMT")

## Adds the time zone
attr(multiYearDF_prep$DateTime, "tzone") <- "Europe/Paris"

# extracts DateTime information into individual attributes
multiYearDF_prep$year <- year(multiYearDF_prep$DateTime)
multiYearDF_prep$quarter <- quarter(multiYearDF_prep$DateTime)
multiYearDF_prep$month <- month(multiYearDF_prep$DateTime)
multiYearDF_prep$week <- week(multiYearDF_prep$DateTime)
multiYearDF_prep$day <- day(multiYearDF_prep$DateTime)
multiYearDF_prep$hour <- hour(multiYearDF_prep$DateTime)
multiYearDF_prep$minute <- minute(multiYearDF_prep$DateTime)

# makes submeter headings more descriptive
multiYearDF_prep <- multiYearDF_prep %>% 
                      rename(
                        kitchen = Sub_metering_1,
                        laundry = Sub_metering_2,
                        heater_and_AC = Sub_metering_3
                      )

# renames months from numbers to names
multiYearDF_prep$month <- multiYearDF_prep$month %>% 
                            recode("1" = "Jan",
                                   "2" = "Feb",
                                   "3" = "Mar",
                                   "4" = "Apr",
                                   "5" = "May",
                                   "6" = "Jun",
                                   "7" = "Jul",
                                   "8" = "Aug",
                                   "9" = "Sep",
                                   "10" = "Oct",
                                   "11" = "Nov",
                                   "12" = "Dec")

# changes submeter names and values to rows instead of columns for easier plotting
multiYearDF_prep <- multiYearDF_prep %>%
  gather(submeter, watt_hr, `kitchen`, `laundry`, `heater_and_AC`) 

# writes dataframe to csv
write.csv(multiYearDF_prep,"data/multiYearDF_prep.csv", row.names = FALSE)

